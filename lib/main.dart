import 'package:flutter/material.dart';
import 'package:responsive_design/desktop/desktop-scaffold.dart';
import 'package:responsive_design/mobile/mobile_scaffold.dart';
import 'package:responsive_design/responsive_layout.dart';
import 'package:responsive_design/tablet/tablet_scaffold.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ResponsiveLayout(
        mobileScaffold: const MobileScaffold(),
        desktopScaffold: const DesktopScaffold(),
        tabletScaffold: const TabletScaffold(),
      ),
    );
  }
}
